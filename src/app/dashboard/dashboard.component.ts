import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { merge } from 'rxjs/operators/merge';
import { toArray } from 'rxjs/operators/toArray';
import { map } from 'rxjs/operators/map';

import { SnapService } from '@sn/core/snap.service';
import { TweetService } from '@sn/core/tweet.service';
import { Publication } from '@sn/core/publication.model';
import { Tweet } from '@sn/tweet/tweet.model';
import { Snap } from '@sn/snap/snap.model';

@Component({
  selector: 'sn-dashboard',
  template: `
    <h1>{{title}}</h1>
    <div *ngFor='let publication of publications | async'>
      <a routerLink="/{{publication.category}}/{{publication.id}}">
      <mat-card>
        <mat-card-content>
          {{publication.id}} - {{publication.content}}
          <img *ngIf="publication.src" src="{{publication.src}}" alt="{{publication.content}}"/>
        </mat-card-content>
      </mat-card>
      </a>
    </div>
  `,
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  title = 'Dashboard';

  publications: Observable<Publication[]>;

  constructor(
    private snapService: SnapService,
    private tweetService: TweetService
  ) { }

  ngOnInit() {
    const tweets: Observable<Tweet> = this.tweetService.getTweets();
    const snaps: Observable<Snap> = this.snapService.getSnaps();

    this.publications = tweets.pipe(
      merge(snaps),
      toArray(),
      map(itemArray => itemArray.sort((firstItem, secondItem) => {
        return (firstItem.content < secondItem.content) ? -1 : (firstItem.content > secondItem.content) ? 1 : 0;
      }))
    );
  }
}
