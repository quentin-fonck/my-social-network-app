import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@sn/shared/shared.module';
import { DashboardRoutingModule } from '@sn/dashboard/dashboard-routing.module';

import { DashboardComponent } from '@sn/dashboard/dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    DashboardRoutingModule
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
