import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sn-tweet',
  template: `
    <h1>{{title}}</h1>
    <router-outlet></router-outlet>
  `,
  styleUrls: ['./tweet.component.scss']
})
export class TweetComponent implements OnInit {
  title = 'Tweets';
  constructor() { }

  ngOnInit() {
  }

}
