import { SharedModule } from '@sn/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TweetComponent } from '@sn/tweet/tweet.component';
import { TweetDetailComponent } from '@sn/tweet/tweet-detail/tweet-detail.component';
import { TweetListComponent } from '@sn/tweet/tweet-list/tweet-list.component';
import { TweetRoutingModule } from '@sn/tweet/tweet-routing.module';

import { TweetService } from '@sn/core/tweet.service';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    TweetRoutingModule
  ],
  declarations: [
    TweetComponent,
    TweetListComponent,
    TweetDetailComponent
  ],
  providers: [TweetService]
})
export class TweetModule { }
