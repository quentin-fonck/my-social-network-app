import { Publication } from '@sn/core/publication.model';

export class Tweet extends Publication {
    constructor(
      public id: string,
      public category: string,
      public content: string,
      public date?: string,
      public username?: string,
      public userId?: string
    ) {
      super(id, category, content, date, username, userId);
    }
}
