import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TweetComponent } from '@sn/tweet/tweet.component';
import { TweetDetailComponent } from '@sn/tweet/tweet-detail/tweet-detail.component';
import { TweetListComponent } from '@sn/tweet/tweet-list/tweet-list.component';

const routes: Routes = [
  {
    path: '', component: TweetComponent,
    children: [
      { path: '', component: TweetListComponent },
      { path: ':id', component: TweetDetailComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TweetRoutingModule { }
