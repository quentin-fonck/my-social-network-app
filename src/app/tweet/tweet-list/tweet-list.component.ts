import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';

import { Tweet } from '@sn/tweet/tweet.model';
import { TweetService } from '@sn/core/tweet.service';

@Component({
  selector: 'sn-tweet-list',
  template: `
    <h2 highlight>Liste des tweets</h2>
    <sn-publication-input></sn-publication-input>
    <div *ngFor='let tweet of tweets'>
      <a routerLink="{{tweet.id}}">
      <mat-card>
        <mat-card-content>
          {{tweet.id}} - {{tweet.content}}
        </mat-card-content>
      </mat-card>
      </a>
    </div>
  `,
  styleUrls: ['./tweet-list.component.scss']
})
export class TweetListComponent implements OnInit {

  tweets: Tweet[] = [];

  constructor(private tweetService: TweetService) { }

  ngOnInit() {
    this.tweetService.getTweets()
    .subscribe(tweet => this.tweets.push(tweet));
  }

}
