import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Tweet } from '@sn/tweet/tweet.model';

import { TweetService } from '@sn/core/tweet.service';

@Component({
  selector: 'sn-tweet-detail',
  template: `
  <div *ngIf="tweet">
    <h2>
      Détail du tweet {{tweet.id}} !
    </h2>
    <p>{{tweet.content}}</p>
    <sn-back text="Retour"></sn-back>
  </div>
  `,
  styleUrls: ['./tweet-detail.component.scss']
})
export class TweetDetailComponent implements OnInit {

  tweet: Tweet;

  constructor(
    private route: ActivatedRoute,
    private tweetService: TweetService
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.tweetService.getTweet(id).subscribe(tweet => this.tweet = tweet);
  }

}
