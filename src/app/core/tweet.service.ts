import { Injectable, OnDestroy } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { from } from 'rxjs/observable/from';
import { delay } from 'rxjs/operators';

import { Tweet } from '@sn/tweet/tweet.model';

const TWEETS: Tweet[] = [
  new Tweet('1', 'tweets', 'Fugiat laboris anim labore eu qui dolor culpa cupidatat.'),
  new Tweet('2', 'tweets', 'Sunt dolore in esse dolore voluptate amet deserunt esse irure voluptate magna officia occaecat qui.'),
  new Tweet('3', 'tweets', 'Ad laborum ullamco deserunt mollit.'),
  new Tweet('4', 'tweets', 'Mollit aute ad do laboris consequat dolore velit Lorem ea reprehenderit ad cupidatat ut.'),
  new Tweet('6', 'tweets', 'Nisi duis minim commodo ullamco irure deserunt ut in sit elit tempor.')
];

const LATENCY = 500;

@Injectable()
export class TweetService implements OnDestroy {

  constructor() {console.log('Service des tweets initialisé !'); }
  ngOnDestroy() {console.log('Service des tweets détruite !'); }

  getTweets(): Observable<Tweet> {
    return from(TWEETS).pipe(delay(LATENCY));
  }

  getTweet(id: string): Observable<Tweet> {
    return of(TWEETS.find(tweet => tweet.id === id))
      .pipe(delay(LATENCY));
  }

}
