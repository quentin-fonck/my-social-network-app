
// Abstract object that define what a publication should have
export abstract class Publication {
  constructor(
    public id: string,
    public category: string,
    public content: string,
    public date?: string,
    public username?: string,
    public userId?: string
  ) {}
}
