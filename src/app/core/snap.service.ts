import { Injectable, OnDestroy } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { from } from 'rxjs/observable/from';
import { delay } from 'rxjs/operators';

import { Snap } from '@sn/snap/snap.model';

const SNAPS: Snap[] = [
  new Snap('1', 'snaps', 'https://i.pinimg.com/736x/22/30/54/22305418269cbc153015646ab1b2e54d--oaxaca-lemurs.jpg', 'A fat chinchilla'),
  new Snap('2', 'snaps', 'http://www.chinchilla-angora.com/images/angora/5%20cocoon.JPG', 'An other chinchilla'),
  new Snap('3', 'snaps', 'https://lazypenguins.com/wp-content/uploads/2016/01/Dogs-in-the-snow-6.jpg', 'A dog in the snow'),
  new Snap('4', 'snaps', 'https://metrouk2.files.wordpress.com/2017/07/187144066.jpg?w=748&h=498&crop=1', 'Some cute kitten')
];

const LATENCY = 500;

@Injectable()
export class SnapService implements OnDestroy {

  constructor() { console.log('Service des Snaps initialisé !'); }
  ngOnDestroy() { console.log('Service des Snaps détruite !'); }


  getSnaps(): Observable<Snap> {
    return from(SNAPS).pipe(delay(LATENCY));
  }
  getSnap(id: string): Observable<Snap> {
    return of(SNAPS.find(snap => snap.id === id))
      .pipe(delay(LATENCY));
  }
}
