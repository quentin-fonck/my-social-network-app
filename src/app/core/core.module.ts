import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TweetService } from '@sn/core/tweet.service';
import { SnapService } from '@sn/core/snap.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [],
  providers: [
    TweetService,
    SnapService
  ],
  exports: [FormsModule]
})
export class CoreModule { }
