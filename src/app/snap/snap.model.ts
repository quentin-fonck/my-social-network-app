import { Publication } from '@sn/core/publication.model';

export class Snap extends Publication {
  constructor(
    public id: string,
    public category: string,
    public src: string,
    public content: string,
    public date?: string,
    public username?: string,
    public userId?: string
  ) {
    super(id, category, content, date, username, userId);
 }
}
