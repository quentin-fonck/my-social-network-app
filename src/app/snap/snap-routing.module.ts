import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SnapComponent } from '@sn/snap/snap.component';
import { SnapListComponent } from '@sn/snap/snap-list/snap-list.component';
import { SnapDetailComponent } from '@sn/snap/snap-detail/snap-detail.component';

const routes: Routes = [
  {
    path: '',
    component: SnapComponent,
    children: [
      { path: '', component: SnapListComponent },
      { path: ':id', component: SnapDetailComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SnapRoutingModule { }
