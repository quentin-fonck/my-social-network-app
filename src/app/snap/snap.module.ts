import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@sn/shared/shared.module';

import { SnapComponent } from '@sn/snap/snap.component';
import { SnapListComponent } from '@sn/snap/snap-list/snap-list.component';
import { SnapDetailComponent } from '@sn/snap/snap-detail/snap-detail.component';
import { SnapRoutingModule } from '@sn/snap/snap-routing.module';
import { SnapService } from '@sn/core/snap.service';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SnapRoutingModule
  ],
  declarations: [
    SnapComponent,
    SnapListComponent,
    SnapDetailComponent
  ],
  providers: [SnapService]
})
export class SnapModule { }
