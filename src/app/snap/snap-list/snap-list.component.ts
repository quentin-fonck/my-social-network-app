import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { SnapService } from '@sn/core/snap.service';
import { Snap } from '@sn/snap/snap.model';

@Component({
  selector: 'sn-snap-list',
  template: `
    <h2 highlight>Liste des Snaps</h2>
    <sn-publication-input></sn-publication-input>
    <mat-grid-list cols="2"  gutterSize="3px">
      <mat-grid-tile *ngFor='let snap of snaps'>
        <a routerLink="{{snap.id}}">
          <img src="{{snap.src}}"/>
          <mat-grid-tile-footer>{{snap.content}}</mat-grid-tile-footer>
      </a>
      </mat-grid-tile>
    </mat-grid-list>
    `,
  styleUrls: ['./snap-list.component.scss']
})
export class SnapListComponent implements OnInit {
  snaps: Snap[] = [];

  constructor(private snapService: SnapService) { }

  ngOnInit() {
    this.snapService.getSnaps()
    .subscribe(snap => this.snaps.push(snap));
  }

}
