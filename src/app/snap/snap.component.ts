import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sn-snap',
  template: `
    <h1>{{title}}</h1>
    <router-outlet></router-outlet>
  `,
  styleUrls: ['./snap.component.scss']
})
export class SnapComponent implements OnInit {
  title = 'Snaps';
  constructor() { }

  ngOnInit() {
  }

}
