import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SnapService } from '@sn/core/snap.service';

import { Snap } from '@sn/snap/snap.model';

@Component({
  selector: 'sn-snap-detail',
  template: `
  <mat-card *ngIf="snap">
  <mat-card-header>
    <mat-card-title><h2>{{snap.content}}</h2></mat-card-title>
  </mat-card-header>
  <img mat-card-image src="{{snap.src}}" alt="{{snap.content}}">
  <sn-back text="Retour"></sn-back>
</mat-card>
`,
  styleUrls: ['./snap-detail.component.scss']
})
export class SnapDetailComponent implements OnInit {

  snap: Snap;

  constructor(
    private route: ActivatedRoute,
    private snapService: SnapService
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.snapService.getSnap(id)
      .subscribe(snap => this.snap = snap);
  }

}
