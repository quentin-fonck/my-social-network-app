import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SnapDetailComponent } from './snap-detail.component';

describe('SnapDetailComponent', () => {
  let component: SnapDetailComponent;
  let fixture: ComponentFixture<SnapDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SnapDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnapDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
