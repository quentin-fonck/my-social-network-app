import { Component } from '@angular/core';

@Component({
  selector: 'sn-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'The Social Network';

  navLinks: object[] = [
    { path: '/dashboard',  label: 'Dashboard'},
    { path: '/tweets', label: 'Tweets'},
    { path: '/snaps', label: 'Snaps'}
  ];
}
