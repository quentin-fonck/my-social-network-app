import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicationInputComponent } from './publication-input.component';

describe('PublicationInputComponent', () => {
  let component: PublicationInputComponent;
  let fixture: ComponentFixture<PublicationInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicationInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicationInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
