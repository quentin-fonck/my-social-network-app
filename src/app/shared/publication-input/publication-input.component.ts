import { Component, EventEmitter, Output, Input } from '@angular/core';
import { NgForm } from '@angular/forms';

import { Publication } from '@sn/core/publication.model';
import { Tweet } from '@sn/tweet/tweet.model';
import { Snap } from '@sn/snap/snap.model';

@Component({
  selector: 'sn-publication-input',
  templateUrl: './publication-input.component.html',
  styleUrls: ['./publication-input.component.scss']
})
export class PublicationInputComponent {


  publication: Publication;

  constructor() {}

  onSubmit(publicationForm: NgForm) {
    console.log(publicationForm);

    alert(`Vous avez écrit: ${publicationForm.controls.content.value}`);
  }

  onClear(messageForm: NgForm) {
    this.publication = null;
    messageForm.resetForm();
  }


}
