import { Component, Input } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'sn-back',
  template: `
  <button mat-raised-button (click)="this.goBack()">
    {{text}}
  </button>
  `})
export class BackComponent {
  @Input() text = 'Retour vers la list';

  constructor(public _location: Location) { }

  goBack(): void {
    this._location.back();
  }
}
