import { NgModule } from '@angular/core';
import { MaterialModule } from '@sn/shared/material.module';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BackComponent } from '@sn/shared/back/back.component';
import { PublicationInputComponent } from '@sn/shared/publication-input/publication-input.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule
  ],
  declarations: [BackComponent, PublicationInputComponent],
  exports: [
    MaterialModule,
    BackComponent,
    PublicationInputComponent
  ]
})
export class SharedModule { }
